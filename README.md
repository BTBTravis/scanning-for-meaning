# Scanning for Meaning

"web app" as art, scanning Wikipedia images for meaning.

**Site:** [scanning-for-meaning.travisshears.xyz](https://scanning-for-meaning.travisshears.xyz/)  
**Blog Post:** https://travisshears.com/blog/scanning-for-meaning/  

## Arch

Scanning for Meaning is broken up into two parts, a frontend react app located in `/web` and a node
js server `/server.js`. The backend is responsible for serving static files then their websocket and
http communication. It also handles all the querying of Wikipedia for random entries with images and
saving their info to a mysql db.

## Dev

Local dev is done via docker so the only dev requirement is working docker setup.

To start the local dev server and accompanying mysql instance run
`$ docker-compose -f docker-compose-dev.yml up -d`. Which starts server accessible on
`http://localhost:80`


The backend will automatically refresh on file change but the frontend needs building on file
change. This can be accomplished with by sshing into the container,
`$ docker exec -it sfm_node_app /bin/bash` then `cd web` and `npm run build`

## Screenshots

![collapsed screenshot](https://travisshears.com/blog/scanning-for-meaning/single.png)

![expanded screenshot](https://travisshears.com/blog/scanning-for-meaning/expanded.png)

![full-screen screenshot](https://travisshears.com/blog/scanning-for-meaning/full-screen.png)
