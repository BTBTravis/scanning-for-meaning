FROM node:current
WORKDIR /app
COPY package.json .
RUN npm i -g forever
RUN npm i
RUN mkdir web
COPY ./web/package.json ./web
RUN (cd ./web && npm i)
ENV NODE_ENV production
COPY . .
RUN (cd ./web && npm run build)
EXPOSE 3000
CMD [ "forever", "server.js" ]
